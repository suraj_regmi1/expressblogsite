const express = require('express');
const passport = require('passport');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const multer = require('multer');
const path = require('path');
// const passportLocal = require('passport-local');
const expressSession = require('express-session');
// const cookieParser = require('cookie-parser');
// const cookieSession = require('cookie-session');
const passportAuthentication = require('./controller/passportAuthentication');

//express initialization
const app = express();

//import all the routes in app(server.js)
let router = require('./routes/index');

//initialize session
app.use(
    expressSession({
        secret: 'secret',
        resave: true,
        saveUninitialized: true
    })
);





//static files
app.use(express.static('public'));




//body parser initialization
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
//passport initialization
app.use(passport.initialize());
app.use(passport.session());


//db connection start
mongoose.connect('mongodb://localhost:27017/mydb1', {
        useNewUrlParser: true
    })
    .then(() => {
        console.log("Connected..")
    })
    .catch(err => console.log("Err", err))
//db connection end

//setting up view engine
app.set('view engine', 'ejs');


//initialize or use the routes here
router(app);



app.listen('3000', (err) => {
    if (err) {
        console.log('failed to connect to port');

    } else {
        console.log('app is listening to port 3000');
    }
});