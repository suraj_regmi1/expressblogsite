let passportSchema = require('../model/passportSchema');
let passport = require('passport');
//passport local authenticaton
const LocalStrategy = require('passport-local').Strategy;

passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    },
    function (email, password, done) {
        passportSchema.findOne({
            email: email
        }, function (err, user) {
            // console.log(user);
            if (err) {
                return done(err);
            }

            if (!user) {
                return done(null, false, {
                    message: 'incorrect username'
                });
            }

            if (user.password != password) {
                return done(null, false, {
                    message: 'incorrect password'
                });
            }
            return done(null, user);
        });
    }
));
//serialization and deserialization
passport.serializeUser((user, done) => {
    // console.log(user.id)
    done(null, user.id);
});

passport.deserializeUser((id, done) => {
    passportSchema.findById(id).then((user) => {
        done(null, user);
    })


});