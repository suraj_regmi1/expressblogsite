require("../controller/passportAuthentication");
let express = require("express");
let passport = require("passport");
let multer = require("multer");
let path = require("path");
let localStrategy = require("passport-local");
const {
    ensureAuthenticated
} = require("../controller/auth");
let mongoose = require("mongoose");


let textSchema = require("../model/textSchema");
let passportSchema = require("../model/passportSchema");
// const express = require('express');
module.exports = app => {
    app.get("/profile", ensureAuthenticated, (req, res) => {
        let blog = textSchema.find((err, blog) => {
            if (err) {
                return err;
            }
            // console.log(blog);
            return blog;


        });

        let user = passportSchema.findById(req.user._id, (err, user) => {
            if (err) {
                return err;
            }
            return user;

        });

        Promise.all([blog, user]).then(result => {
            res.render("profile", {
                blogs: result[0],
                user: result[1]
            });
        });
    });

    //logout routes
    app.get("/logout", (req, res) => {
        //handle  with passport
        req.logOut();
        res.redirect("/");
    });

    // // ...........fileupload section of code.................

    let storage = multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, './public/uploads');
        },
        filename: function (req, file, cb) {
            const ext = file.mimetype.split('/')[1];
            cb(null, file.fieldname + '-' + Date.now() + '.' + ext)
        }
    });

    let upload = multer({
        storage: storage
    })
    //...........file upload section of code ends here...........

    app.post("/profile", upload.single('myImage'), (req, res) => {

        // console.log(req.file);

        // console.log(req.body);


        //saving to database.....
        req.body.userId = mongoose.Types.ObjectId(req.user._id);

        // saving image url path to mongoose schema
        req.body.myImage = 'http://localhost:3000/uploads/' + req.file.filename;

        let myData = new textSchema(req.body);
        // console.log(myData);
        myData
            .save()
            .then(item => {
                // console.log(item);
                res.redirect("/profile");
            })
            .catch(err => {
                res.status(400).send("unable to save to database");
                console.log(err);
            });

    });
};