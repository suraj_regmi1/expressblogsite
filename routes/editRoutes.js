let inputText = require('../model/textSchema');
module.exports = (app) => {
    app.get('/edit/:id', function (req, res) {
        // console.log(req.params.id)

        inputText.findById(req.params.id, function (err, blog) {
            if (err) {
                console.log(err);

            } else {
                console.log(blog);
                res.render('edit', {
                    blog: blog
                });
            }
        });


    });
    //handle edit using post

    app.post('/edit/:id', function (req, res) {
        // console.log('MyId id' + req.params.id);

        const mybodydata = {
            inputText: req.body.inputText,


        }

        inputText.findByIdAndUpdate(req.params.id, mybodydata, function (err) {
            if (err) {
                res.redirect('/edit' + req.params.id);
            } else {
                res.redirect('/profile');
            }
        });

    });

}