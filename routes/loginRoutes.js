// const express = require('express');
let passportAuthentication = require('../controller/passportAuthentication');
let passport = require('passport');
let passportLocal = require('passport-local');

module.exports = (app) => {
    app.get('/login', (req, res) => {
        res.render('login');
    });

    app.post('/login',
        passport.authenticate('local', {
            failureRedirect: '/error',
        }),
        function (req, res) {
            res.redirect('/profile');
        });




}