let homeRoutes = require('./homeRoute');
let loginroutes = require('./loginRoutes');
let signupRoutes = require('./signupRoutes');
let profileRoutes = require('./profileRoutes');
let displayRoutes = require('./displayRoutes');
let editRoutes = require('./editRoutes');
let deleteRoutes = require('./deleteRoutes');
let userExistRoutes = require('./userExist');
let contactRoutes = require('./contactRoutes');
let postRoutes = require('./postRoutes');




module.exports = (app) => {
    homeRoutes(app)
    loginroutes(app)
    signupRoutes(app)
    profileRoutes(app)
    displayRoutes(app)
    editRoutes(app)
    deleteRoutes(app)
    userExistRoutes(app)
    contactRoutes(app)
    postRoutes(app)
    app.get('*', (req, res) => {
        res.send('page not found  404 error')
    });

};