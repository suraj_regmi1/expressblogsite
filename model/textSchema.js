const mongoose = require('mongoose');
let Schema = mongoose.Schema;

let textSchema = new Schema({
    inputText: {
        type: String,
        required: true

    },
    title: {
        type: String,
        required: true
    },

    date: {
        type: Date,
        'default': Date.now,
        index: true
    },
    myImage: {
        type: String,
        required: true
    },
    userId: {
        type: String
    }







});
module.exports = mongoose.model('textInput', textSchema);